import { React, useState } from 'react'
import { Button, Box, Modal, TextField, Grid } from '@mui/material'
import { borderRadius } from '@mui/system';
import Search from './Search';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import Alert from '@mui/material/Alert';

const AddButton = () => {
    const [trigger, setTrigger] = useState(false);
    const [edit, setEdit] = useState(false)
    const [info, setInfo] = useState({
        name: '',
        address: '',
        hobby: ''
    });
    const [search, setSearch] = useState('')
    const [data, setData] = useState([]);
    const [error, setError] = useState('');
    const [selectedData, setSelectedData] = useState({
        id: 0,
        name: '',
        address: '',
        hobby: ''
    })

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const result = data.filter((x) => x.name.includes(search.toLowerCase() || x.address.includes(search.toLowerCase())))

    const handleEdit = (id, name, address, hobby) => {
        setSelectedData({ id, name, address, hobby });
        console.log("AAAAAAAA", id, name, address, hobby)
        setEdit(true)
    }


    return (
        <>
            <div className='navbar'>
                <Box display='flex' justifyContent='space-between'>
                    <span>My App</span>
                    <Button onClick={() => { setTrigger(true) }} sx={{ borderRadius: 28 }} variant="contained" >Add User</Button>
                </Box>
            </div>



            <div>

                <Modal
                    open={trigger}
                    onClose={() => setTrigger(false)}
                    aria-labelledby="parent-modal-title"
                    aria-describedby="parent-modal-description">
                    <Box sx={{ ...style, width: 400 }}>
                        {error ? <Alert severity="error">{error} field cannot be empty</Alert>
                            : <></>}
                        <label>Name</label>
                        <TextField required sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, name: e.target.value })} />
                        <label>Address</label>
                        <TextField required sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, address: e.target.value })} />
                        <label>Hobby</label>
                        <TextField required sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setInfo({ ...info, hobby: e.target.value })} />
                        <div className='btn-save'>
                            <Button variant='contained' onClick={() => {
                                if (info.name == "") {
                                    setError("Name");
                                }
                                else if (info.address == "") {
                                    setError("Adress");
                                }
                                else if (info.hobby == "") {
                                    setError("Hobby");
                                }
                                else {
                                    setError("");
                                    setData((prev) => [...prev, info])
                                    setTrigger(false)
                                    setInfo({
                                        name: '',
                                        address: '',
                                        hobby: ''
                                    });
                                }
                            }}>Save</Button>
                        </div>

                    </Box>
                </Modal>

            </div >

            {(data.length === 0) ?
                <div className='text'>
                    <h1>
                        O User
                    </h1>
                </div>
                :
                <div>
                    <div>
                        <div className='search'>
                            <TextField onChange={(e) => { setSearch(e.target.value) }} sx={{ borderRadius: '50%' }} placeholder="Search" />
                        </div>

                        <Modal
                            open={edit}
                            onClose={() => setEdit(false)}
                            aria-labelledby="parent-modal-title"
                            aria-describedby="parent-modal-description">
                            <Box sx={{ ...style, width: 400 }}>
                                {error ? <Alert severity="error">{error} field cannot be empty</Alert>
                                    : <></>}
                                <label>Name</label>
                                <TextField required value={selectedData.name} sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setSelectedData({ ...selectedData, name: e.target.value })} />
                                <label>Address</label>
                                <TextField required value={selectedData.address} sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setSelectedData({ ...selectedData, address: e.target.value })} />
                                <label>Hobby</label>
                                <TextField required value={selectedData.hobby} sx={{ borderRadius: 28, paddingBottom: '10px', paddingTop: '10px' }} fullWidth variant='outlined' onChange={(e) => setSelectedData({ ...selectedData, hobby: e.target.value })} />
                                <div className='btn-save'>
                                    <Button variant='contained' onClick={() => {
                                        if (selectedData.name == "") {
                                            setError("Name");
                                        }
                                        else if (selectedData.address == "") {
                                            setError("Adress");
                                        }
                                        else if (selectedData.hobby == "") {
                                            setError("Hobby");
                                        }
                                        else {
                                            setError("");
                                            setData((prev) => {
                                                let tmp = prev;
                                                tmp[selectedData.id] = { name: selectedData.name, address: selectedData.address, hobby: selectedData.hobby };
                                                return [...tmp]
                                            }); setEdit(false)
                                        }

                                    }}>Save</Button>
                                </div>

                            </Box>
                        </Modal>

                    </div >
                    {search ? result.map((e, index) => {
                        return (
                            <>
                                <Box class="container" minWidth={256} bgcolor={'#fff'}>
                                    <List>
                                        <ListItem
                                        >
                                            <ListItemText key={e.name} primary={e.name} secondary={e.address} />
                                            <ListItemText key={e.hobby} primary={e.hobby} secondary={<Button onClick={() => { handleEdit(index, e.name, e.address, e.hobby) }} size="small" sx={{ borderRadius: 28 }} variant="contained" >Edit</Button>} />
                                        </ListItem>

                                    </List>
                                </Box>

                            </>
                        );
                    }) : data.map((e, index) => {
                        return (
                            <>
                                <Box class="container" minWidth={256} bgcolor={'#fff'}>
                                    <List dense sx={{ width: '100%', bgcolor: 'background.paper' }}>
                                        <ListItem
                                        >
                                            <ListItemText key={e.name} primary={e.name} secondary={e.address} />
                                            <ListItemText key={e.hobby} primary={e.hobby} secondary={<Button onClick={() => { handleEdit(index, e.name, e.address, e.hobby) }} size="small" sx={{ borderRadius: 28 }} variant="contained" >Edit</Button>} />
                                        </ListItem>

                                    </List>
                                </Box>



                            </>
                        );
                    })}

                </div>
            }

        </>
    )
}

export default AddButton